FROM registry.gitlab.com/thesteg/node-webserver:latest
COPY build build
EXPOSE 5000
CMD serve -s build